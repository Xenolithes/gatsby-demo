import React from "react"
import { graphql, useStaticQuery, Link } from "gatsby"

export default () => {
  const query = useStaticQuery(graphql`
    query {
      allMdx {
        nodes {
          frontmatter {
            date
            path
            title
          }
        }
      }
    }
  `)

  return (
    <div>
      Hello world!
      <ul>
        {query.allMdx.nodes.map(node => {
          return (
            <Link key={node.frontmatter.title} to={node.frontmatter.path}>
              <li>
                <h2>{node.frontmatter.title}</h2>
              </li>
            </Link>
          )
        })}
      </ul>
    </div>
  )
}
